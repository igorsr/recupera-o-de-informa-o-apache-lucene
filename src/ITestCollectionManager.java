import java.util.ArrayList;

import org.apache.lucene.document.Document;

public interface ITestCollectionManager {

	void IndexDocuments(String path, ITextIndexer indexer);	
}
