import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;

public interface ITextIndexer {
	
	void CreateIndexRAM();
	void CreateIndexFile(String dirPath);
	void AddDoc(Document doc) throws IOException;
	void CloseIndex() throws IOException;
	ArrayList<Point> QueryResultMatch(String filePath, int queryId, ArrayList<Integer> resultDocs) throws IOException;
	ArrayList<Point> Query(String query, int queryId, String filePathRels) throws IOException, ParseException;
	Analyzer GetAnalyzer();
	
}
