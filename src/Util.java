import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import snowballstemmer.PorterStemmer;

public final class Util {

  private Util() {}

  public static String stemString(Analyzer analyzer, String string) {
	
    List<String> result = new ArrayList<String>();
    try {
      TokenStream stream  = analyzer.tokenStream(null, new StringReader(string));
      stream.reset();
      while (stream.incrementToken()) {
        result.add(stream.getAttribute(CharTermAttribute.class).toString());
      }
      
      stream.close();
      
    } catch (IOException e) {
    }
    finally{
    	
    }
    
    String resultString  = "";
    
    PorterStemmer stemmer = new PorterStemmer();    
    
    for(String s : result){    		
    	stemmer.setCurrent(s);
    	stemmer.stem();
    	resultString+=stemmer.getCurrent() + " ";
    }   
     
    return resultString;
  }

}