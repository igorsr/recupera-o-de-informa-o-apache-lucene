import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;

public class TestCollectionManager implements ITestCollectionManager {

	private static String filepath;

	private static List<String> readFileLines(String path) throws IOException{		
		return Files.readAllLines(Paths.get(path), Charset.defaultCharset());		
	}		

	@Override
	public void IndexDocuments(String path, ITextIndexer indexer) {

		try {
			
			filepath = path;
			List<String> linhas = readFileLines(filepath);			
			Document doc = null;
			
			String fieldName = "";
			String fieldContent = "";		

			for(String s : linhas){
				
				if(s.contains(".I ")){
					
					if(fieldName!= ""){
						
						TextField f = new TextField(fieldName, fieldContent, Field.Store.YES);					
						doc.add(f);			
						//System.out.println("Campo = "+fieldName+", valor ="+ fieldContent);
					}							
					
					if(doc != null){

						indexer.AddDoc(doc);						
						fieldName = "";						
					}
					
					doc = new Document();
					doc.add(new StringField("id",s.substring(2), Store.YES));
					
				}
				else if(s.charAt(0) == '.' && Character.isLetter(s.charAt(1))){
					
					if(fieldName!= ""){
					
						TextField f = new TextField(fieldName, fieldContent, Field.Store.YES);					
						doc.add(f);			
						//System.out.println("Campo = "+fieldName+", valor ="+ fieldContent);
					}							
					
					fieldName = Character.toString(s.charAt(1));	
					fieldContent = "";
 					
				}
				else{
					fieldContent+=s + "\n";					
				}				
				
				
	        }
			
			if(doc != null){
				indexer.AddDoc(doc);										
			}
			
 
		} catch (IOException e) {

			System.out.println("Erro ao ler o arquivo "+ filepath + " " + e.getMessage());
	    	System.exit(-1);
		} 

	}

}
