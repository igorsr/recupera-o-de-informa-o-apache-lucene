import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.similarities.DefaultSimilarity;


public class CustomSimilarity extends DefaultSimilarity{

	
	@Override
	public float idf(long docFreq, long numDocs) {
		
		return super.idf(docFreq, numDocs);
	}
	
	@Override
	public float tf(float freq) {
		return super.tf(freq);
	}
	
	@Override
	public float lengthNorm(FieldInvertState arg0) {
				
		return super.lengthNorm(arg0);
	}
	
	
}
