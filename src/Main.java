import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.util.Version;

public class Main {

	public static void main(String[] args) throws IOException, ParseException {

		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47);		
		//Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_47);
		//Analyzer analyzer = new SnowballAnalyzer(Version.LUCENE_47, "Porter");
		//Analyzer analyzer = new SimpleAnalyzer(Version.LUCENE_47);
		//Analyzer analyzer = new StopAnalyzer(Version.LUCENE_47, new File ("cacm\\common_words"));
		
		ITextIndexer indexer = new TextIndexer(analyzer);
		indexer.CreateIndexFile("index\\index_file");
		
		//TestCollectionManager collectionManager = new TestCollectionManager();
		ITestCollectionManager collectionManager = new TestCollectionManager();
		collectionManager.IndexDocuments("cacm\\cacm.all", indexer);
		
		//collectionManager.IndexDocuments("cran\\cran.all.1400", indexer);
		
		indexer.CloseIndex();
		
		IQuerySearcher queryReader = new QuerySearcher();
		
		//apaga o arquivo de resultados da query criado anteriormente
		File f = new File("queryResult.txt");
		f.delete();
		
		queryReader.ReadQueries("cacm\\query.text", "cacm\\qrels.text", indexer);		
		//queryReader.ReadQueries("cran\\cran.qry", "cran\\cranqrel", indexer);
		
	}
}