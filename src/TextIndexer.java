import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;

public class TextIndexer implements ITextIndexer {

	private static FSDirectory dir;
	private static RAMDirectory dirRAM;
	private static IndexWriter indexWriter;	
	private Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47);

	public TextIndexer(Analyzer analyzer){
		this.analyzer = analyzer;		
	}
	
	private static String filepath;
	
	private static List<String> readFileLines(String path) throws IOException{		
		return Files.readAllLines(Paths.get(path), Charset.defaultCharset());		
	}

	public static String getFilepath() {
		return filepath;
	}

	public static void setFilepath(String filepath) {
		TextIndexer.filepath = filepath;
	}

	@Override
	public void CreateIndexRAM() {

	    try {
	    	
	    	dirRAM = new RAMDirectory();
	    	IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_47, analyzer);
	    	indexWriter = new IndexWriter(dir, config);     
	      
	    } catch (Exception ex) {
	    	System.out.println("Erro ao criar �ndice " + ex.getMessage());
	    	System.exit(-1);	    	
	    }
	}

	@Override
	public void CreateIndexFile(String dirPath) {

		try {
		    	
			dir = FSDirectory.open(new File(dirPath));	    	    
	    	IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_47, analyzer);
	    	indexWriter = new IndexWriter(dir, config);
	    	indexWriter.deleteAll();
	    	
	    } catch (Exception ex) {
	    	System.out.println("Erro ao criar índice " + ex.getMessage());
	    	System.exit(-1);	    	
	    }
		
	}

	@Override
	public void AddDoc(Document doc) throws IOException {
		try {
		
			indexWriter.addDocument(doc);
		
		}
		catch(Exception ex){
			System.out.println("Erro ao adicionar documento ao índice. " + ex.getMessage());
	    	System.exit(-1);	 
		}
	}

	@Override
	public void CloseIndex() throws IOException {
		indexWriter.close();
	}

	@Override
	public ArrayList<Point> Query(String query, int queryId, String filePathRels) throws IOException, ParseException {

		IndexReader reader = DirectoryReader.open(dir);
		
		IndexSearcher searcher = new IndexSearcher(reader);
		
		//searcher.setSimilarity( new BM25Similarity() );		
		
		searcher.setSimilarity(new CustomSimilarity());		
		
	    String[] fields = new String[]{"T", "A", "B", "C", "N", "X", "W"};
	    
	   int count = 0;
	    
	    ArrayList<Integer> documentosRelevantes = new ArrayList<Integer>();	    	  
		
	    for(String field: fields){    	
	    	
	    	Query q = new QueryParser(Version.LUCENE_47, field, analyzer).parse(QueryParser.escape(query));		    
	    	
	    	//q.setBoost(boosts[count]);
	    	
    	    TopScoreDocCollector collector = TopScoreDocCollector.create(10, true);	
	    	searcher.search(q, collector);
	    	
	    	ScoreDoc[] hits = collector.topDocs().scoreDocs;	    
	        
	        for(int i=0;i<hits.length;i++) {
	        	int docId = hits[i].doc;
	        	Document d = searcher.doc(docId);
	        	// System.out.println((i + 1) + ". " + d.get("id") +  "\t"+ d.get("T") + "score=" + hits[i].score);
	        	documentosRelevantes.add(Integer.parseInt(d.get("id").trim()));	          
	        }	
	        
	        count++;
	    }
	    
	    //Remove duplicados
	    HashSet hs = new HashSet();
	    hs.addAll(documentosRelevantes);
	    documentosRelevantes.clear();
	    documentosRelevantes.addAll(hs);
	    
	    Collections.sort(documentosRelevantes);
	    return QueryResultMatch(filePathRels,queryId,documentosRelevantes);		
	}

	@Override
	public ArrayList<Point> QueryResultMatch(String filePath, int queryId,
			ArrayList<Integer> resultDocs) throws IOException {

		this.setFilepath(filePath);
		
		List<String> linhas = readFileLines(getFilepath());
		
		float countI = 0;//contador de interse��o
		float countR = 0;//contador de respostas ideais
		
		ArrayList<Point> resultados = new ArrayList<Point>();
		
		for(String s : linhas){
			String quebra[] = s.split(" ");
			if(Integer.parseInt(quebra[0].trim())==queryId){
				
				//incrementa o contador de respostas ideais p/ a query
				countR++;
				
				if(resultDocs.size()>0 && resultDocs.contains(Integer.parseInt(quebra[1].trim()))){
					countI++;//incrementa o contador de interse��o
					// -> P/R individual por doc da interse��o 
					int ix = resultDocs.indexOf(Integer.parseInt(quebra[1].trim()))+1;
					
					float precision = (ix>0? (countI/ix): 0)*100;
					float recall = (resultDocs.size()>0? (countI/resultDocs.size()): 0)*100;

					resultados.add(new Point(precision, recall));
					
					System.out.println("Rank "+ix+
							" Precision: "+precision+"%"+
							" Recall: "+recall+"%");
					
				}
			}
		}
		
		//Sorting
		Collections.sort(resultados, new Comparator<Point>() {
		        @Override
		        public int compare(Point  p1, Point  p2)
		        {
		            return  p1.Recall < p2.Recall ? 1 : 0;
		        }
	    });
		
		ArrayList<Point> pontos = Point.Init();		
		
		for(int i=0; i < pontos.size()-1; i++){			
			pontos.get(i).Precision = getMaxPrecision(resultados, i);
		}	
		
		return pontos;
		
	}
	
	private float getMaxPrecision(ArrayList<Point> resultados, int i) {
		
		if(resultados.size()>0 && i <  resultados.size()){
		
			float max = resultados.get(i).Precision;
			
			for(int j = i+1; j< resultados.size(); j++){
				
				if(resultados.get(j).Precision > max){
					max = resultados.get(j).Precision;
				}
			}
			
			return max;
		}
		else{
			return 0;			
		}
	}

	private void appendResultFile(String filePath, int queryId, float precision, float recall) throws IOException{
		File f = new File(filePath);
		
		if(!f.exists()) f.createNewFile();
		
		FileWriter fileWritter = new FileWriter(f.getName(),true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        bufferWritter.write(queryId+" "+precision+ " "+recall+"\n");
        bufferWritter.close();
	}

	@Override
	public Analyzer GetAnalyzer() {
		return this.analyzer;
	}

}
