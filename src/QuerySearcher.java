import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;


public class QuerySearcher implements IQuerySearcher {

	private static String filepath;
	
	private static List<String> readFileLines(String path) throws IOException{		
		return Files.readAllLines(Paths.get(path), Charset.defaultCharset());		
	}

	public static String getFilepath() {
		return filepath;
	}

	public static void setFilepath(String filepath) {
		QuerySearcher.filepath = filepath;
	}

	@Override
	public void ReadQueries(String filePath, String filePathRels, ITextIndexer indexer)
			throws ParseException {

		ArrayList<QueryResult> resultados = new ArrayList<QueryResult>();
				
		try {
			
			this.setFilepath(filePath);
			
			List<String> linhas = readFileLines(getFilepath());			
			
			String query = "";
			int queryNumber = 0;

			for(String s : linhas){
				
				if(s.contains(".I ")){				
					
					if(query != ""){
						
						//Executa a consulta
						System.out.println(query);
						
						QueryResult qr = new QueryResult(indexer.Query(query, queryNumber, filePathRels));
						resultados.add(qr);
					}

					queryNumber = Integer.parseInt(s.substring(2).trim());						
				
				}
				else if(s.startsWith(".W")){
					query = "";
				}
				else if(s.startsWith(".N") || s.startsWith(".A")){

					
				}
				else{
					query += s;				
				}
			}
						
			CalcMedia(resultados, "queryResult.txt", queryNumber);		
		
 
		} catch (IOException e) {

			System.out.println("Erro ao ler o arquivo "+ getFilepath() + " " + e.getMessage());
	    	System.exit(-1);
		}	

	}

	private void CalcMedia(ArrayList<QueryResult> resultados, String filePath, int size) throws IOException {

		//System.out.println("Tamanho = " + size);		
		
		File f = new File(filePath);		
		if(!f.exists()) f.createNewFile();
		FileWriter fileWritter = new FileWriter(f.getName(),true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        
		ArrayList<Point> pontosGrafico = Point.Init();				
			
		for(int j = 0; j < pontosGrafico.size(); j++ ){
				
			for(int i = 0; i < resultados.size(); i++ ){		
				
				QueryResult qr = resultados.get(i);
				
				pontosGrafico.get(j).Precision += (qr.pontos.get(j).Precision)/size;				
				
			}
			
			bufferWritter.write(pontosGrafico.get(j).Precision+" " +pontosGrafico.get(j).Recall + "\n");	
			
		}

        bufferWritter.close();		
		
	}

}
