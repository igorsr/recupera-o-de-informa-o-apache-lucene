import java.util.ArrayList;

public class Point {
	
	public float Precision;
	public float Recall;
	
	public Point(float p, float r){
		this.Precision = p;
		this.Recall = r;		
	}
	
	public static ArrayList<Point>Init(){
		  
	    ArrayList<Point> pontos = new ArrayList<Point>();
	    
	    pontos.add(new Point(0,0));
	    pontos.add(new Point(0,1));
	    pontos.add(new Point(0,2));
	    pontos.add(new Point(0,3));
	    pontos.add(new Point(0,4));
	    pontos.add(new Point(0,5));
	    pontos.add(new Point(0,6));
	    pontos.add(new Point(0,7));
	    pontos.add(new Point(0,8));
	    pontos.add(new Point(0,9));
	    pontos.add(new Point(0,10));	    
	    return pontos;		
	}
	
}