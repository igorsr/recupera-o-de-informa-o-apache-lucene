import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;


public interface IQuerySearcher {

	void ReadQueries(String filePath, String filePathRels, ITextIndexer indexer) throws ParseException;
	
}
